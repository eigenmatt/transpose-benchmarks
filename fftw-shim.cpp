/*
fftw_plan_guru_r2r
fftw_init_threads
fftw_plan_with_nthreads
fftw_import_wisdom_from_filename
fftw_export_wisdom_to_filename
fftw_execute_r2r
*/

#include "fftw-shim.h"

shim_fftw_plan shim_fftw_plan_guru_r2r(int rank, const shim_fftw_iodim *dims,
	int howmany_rank,
	const shim_fftw_iodim *howmany_dims,
	double *in, double *out,
	const shim_fftw_r2r_kind *kind,
	unsigned flags) {
		return fftw_plan_guru_r2r(rank, dims, howmany_rank, howmany_dims, in, out, kind, flags);
}

int shim_fftw_init_threads(void) {
	return fftw_init_threads();
}

void shim_fftw_plan_with_nthreads(int nthreads) {
	fftw_plan_with_nthreads(nthreads);
}

int shim_fftw_import_wisdom_from_filename(const char *filename) {
	return fftw_import_wisdom_from_filename(filename);
}

int shim_fftw_export_wisdom_to_filename(const char *filename) {
	return fftw_export_wisdom_to_filename(filename);
}

void shim_fftw_execute_r2r(const fftw_plan p, double *in, double *out) {
	fftw_execute_r2r(p, in, out);
}

void* shim_fftw_malloc(size_t n) {
	return fftw_malloc(n);
}

void shim_fftw_free(void* ptr) {
	fftw_free(ptr);
}