cmake_minimum_required(VERSION 3.3)

project(
		transpose-bench
		VERSION 1.0
		LANGUAGES CXX C)

find_package(benchmark REQUIRED)

if(NOT DEFINED USE_MKL)
	set(USE_MKL OFF BOOL "use MKL instead of FFTW")
endif()
if(USE_MKL)
	find_package(MKL CONFIG REQUIRED)
endif()

find_package(Eigen3 3.4 REQUIRED NO_MODULE)

add_library(fftw-shim fftw-shim.cpp)
target_link_libraries(fftw-shim fftw3_threads)
target_link_libraries(fftw-shim fftw3)

add_library(ulvt-transpose ulvt-transpose.c)
target_compile_options(ulvt-transpose PUBLIC -mavx)

add_executable(transpose-benchmarks main.cpp)

target_link_libraries(transpose-benchmarks fftw-shim ulvt-transpose)
target_link_libraries(transpose-benchmarks benchmark::benchmark)
target_link_libraries(transpose-benchmarks benchmark::benchmark_main)

if(USE_MKL)
	target_compile_options(transpose-benchmarks PUBLIC $<TARGET_PROPERTY:MKL::MKL,INTERFACE_COMPILE_OPTIONS>)
	target_include_directories(transpose-benchmarks PUBLIC $<TARGET_PROPERTY:MKL::MKL,INTERFACE_INCLUDE_DIRECTORIES>)
	target_link_libraries(transpose-benchmarks $<LINK_ONLY:MKL::MKL>)
	target_compile_definitions(transpose-benchmarks PRIVATE USE_MKL=1)
endif()
find_package(OpenMP REQUIRED)
target_link_libraries(transpose-benchmarks OpenMP::OpenMP_CXX)
target_link_libraries(transpose-benchmarks Eigen3::Eigen)
target_link_libraries(transpose-benchmarks ${CMAKE_SOURCE_DIR}/hptt/lib/libhptt.so)
target_include_directories(transpose-benchmarks PRIVATE ${CMAKE_SOURCE_DIR}/hptt/include/)
