#pragma once

#include <fftw3.h>

typedef fftw_plan shim_fftw_plan;
typedef fftw_iodim shim_fftw_iodim;
typedef fftw_r2r_kind shim_fftw_r2r_kind;

shim_fftw_plan shim_fftw_plan_guru_r2r(int rank, const fftw_iodim *dims,
	int howmany_rank,
	const fftw_iodim *howmany_dims,
	double *in, double *out,
	const fftw_r2r_kind *kind,
	unsigned flags);

int shim_fftw_init_threads(void);
void shim_fftw_plan_with_nthreads(int nthreads);

int shim_fftw_import_wisdom_from_filename(const char *filename);
int shim_fftw_export_wisdom_to_filename(const char *filename);

void shim_fftw_execute_r2r(const fftw_plan p, double *in, double *out);

void* shim_fftw_malloc(size_t n);
void shim_fftw_free(void* ptr);