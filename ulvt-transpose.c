#include <immintrin.h>

#define COLS           672
#define ROWS           8388608UL
#define ROW_SIZE       (COLS*8)
#define COL_SIZE       (ROWS*8)
#define IN_MATRIX_SIZE (COLS*ROWS*8)

#define COMPILER_BARRIER() asm volatile("")

void ulvt_transpose_v1(void *out, void *in)
{
    __m256d y0, y1, y2, y3, y4, y5, y6, y7, y8, y9, y10, y11, y12, y13;
    size_t counti = 0;

    for (size_t count = IN_MATRIX_SIZE/0x200; count > 0; count--)
    {
        // prefetch for next iteration
        _mm_prefetch(in + 8*ROW_SIZE + 0*ROW_SIZE, _MM_HINT_T0);
        _mm_prefetch(in + 8*ROW_SIZE + 1*ROW_SIZE, _MM_HINT_T0);
        _mm_prefetch(in + 8*ROW_SIZE + 2*ROW_SIZE, _MM_HINT_T0);
        _mm_prefetch(in + 8*ROW_SIZE + 3*ROW_SIZE, _MM_HINT_T0);
        _mm_prefetch(in + 8*ROW_SIZE + 4*ROW_SIZE, _MM_HINT_T0);
        _mm_prefetch(in + 8*ROW_SIZE + 5*ROW_SIZE, _MM_HINT_T0);
        _mm_prefetch(in + 8*ROW_SIZE + 6*ROW_SIZE, _MM_HINT_T0);
        _mm_prefetch(in + 8*ROW_SIZE + 7*ROW_SIZE, _MM_HINT_T0);

        COMPILER_BARRIER();

        // Load first two 4x4 blocks
        // A B C D
        // E F G H
        // I J K L
        // M N O P
        // a b c d
        // e f g h
        // i j k l
        // m n o p
        y0 = _mm256_load_pd((double *)(in + 0*ROW_SIZE + 0x00));
        y1 = _mm256_load_pd((double *)(in + 1*ROW_SIZE + 0x00));
        y2 = _mm256_load_pd((double *)(in + 2*ROW_SIZE + 0x00));
        y3 = _mm256_load_pd((double *)(in + 3*ROW_SIZE + 0x00));
        y4 = _mm256_load_pd((double *)(in + 4*ROW_SIZE + 0x00));
        y5 = _mm256_load_pd((double *)(in + 5*ROW_SIZE + 0x00));
        y6 = _mm256_load_pd((double *)(in + 6*ROW_SIZE + 0x00));
        y7 = _mm256_load_pd((double *)(in + 7*ROW_SIZE + 0x00));

        // Transpose first two columns
        // construct A B I J and E F M N
        y8 = _mm256_permute2f128_pd(y0, y2, 0x20);
        y9 = _mm256_permute2f128_pd(y1, y3, 0x20);
        // construct a b i j and e f m n
        y10 = _mm256_permute2f128_pd(y4, y6, 0x20);
        y11 = _mm256_permute2f128_pd(y5, y7, 0x20);
        // construct A E I M and a e i m
        y12 = _mm256_unpacklo_pd(y8, y9);
        _mm256_stream_pd((double *)(out + 0*COL_SIZE + 0x00), y12);
        y13 = _mm256_unpacklo_pd(y10, y11);
        _mm256_stream_pd((double *)(out + 0*COL_SIZE + 0x20), y13);
        // construct B F J N and b f j n
        y12 = _mm256_unpackhi_pd(y8, y9);
        _mm256_stream_pd((double *)(out + 1*COL_SIZE + 0x00), y12);
        y13 = _mm256_unpackhi_pd(y10, y11);
        _mm256_stream_pd((double *)(out + 1*COL_SIZE + 0x20), y13);

        // Transpose second two columns
        // construct C D K L and G H O P
        y8 = _mm256_permute2f128_pd(y0, y2, 0x31);
        y9 = _mm256_permute2f128_pd(y1, y3, 0x31);
        // construct c d k l and g h o p
        y10 = _mm256_permute2f128_pd(y4, y6, 0x31);
        y11 = _mm256_permute2f128_pd(y5, y7, 0x31);
        // construct C G K O and c g k o
        y12 = _mm256_unpacklo_pd(y8, y9);
        _mm256_stream_pd((double *)(out + 2*COL_SIZE + 0x00), y12);
        y13 = _mm256_unpacklo_pd(y10, y11);
        _mm256_stream_pd((double *)(out + 2*COL_SIZE + 0x20), y13);
        // construct D H L P and d h l p
        y12 = _mm256_unpackhi_pd(y8, y9);
        _mm256_stream_pd((double *)(out + 3*COL_SIZE + 0x00), y12);
        y13 = _mm256_unpackhi_pd(y10, y11);
        _mm256_stream_pd((double *)(out + 3*COL_SIZE + 0x20), y13);

        COMPILER_BARRIER();

        // Prefetch two iterations ahead
        _mm_prefetch(in + 16*ROW_SIZE + 0*ROW_SIZE, _MM_HINT_T0);
        _mm_prefetch(in + 16*ROW_SIZE + 1*ROW_SIZE, _MM_HINT_T0);
        _mm_prefetch(in + 16*ROW_SIZE + 2*ROW_SIZE, _MM_HINT_T0);
        _mm_prefetch(in + 16*ROW_SIZE + 3*ROW_SIZE, _MM_HINT_T0);
        _mm_prefetch(in + 16*ROW_SIZE + 4*ROW_SIZE, _MM_HINT_T0);
        _mm_prefetch(in + 16*ROW_SIZE + 5*ROW_SIZE, _MM_HINT_T0);
        _mm_prefetch(in + 16*ROW_SIZE + 6*ROW_SIZE, _MM_HINT_T0);
        _mm_prefetch(in + 16*ROW_SIZE + 7*ROW_SIZE, _MM_HINT_T0);
 
        COMPILER_BARRIER();

        // Repeat for second half of input cache lines
        y0 = _mm256_load_pd((double *)(in + 0*ROW_SIZE + 0x20));
        y1 = _mm256_load_pd((double *)(in + 1*ROW_SIZE + 0x20));
        y2 = _mm256_load_pd((double *)(in + 2*ROW_SIZE + 0x20));
        y3 = _mm256_load_pd((double *)(in + 3*ROW_SIZE + 0x20));
        y4 = _mm256_load_pd((double *)(in + 4*ROW_SIZE + 0x20));
        y5 = _mm256_load_pd((double *)(in + 5*ROW_SIZE + 0x20));
        y6 = _mm256_load_pd((double *)(in + 6*ROW_SIZE + 0x20));
        y7 = _mm256_load_pd((double *)(in + 7*ROW_SIZE + 0x20));

        // Transpose first two columns
        y8 = _mm256_permute2f128_pd(y0, y2, 0x20);
        y9 = _mm256_permute2f128_pd(y1, y3, 0x20);
        y10 = _mm256_permute2f128_pd(y4, y6, 0x20);
        y11 = _mm256_permute2f128_pd(y5, y7, 0x20);
        y12 = _mm256_unpacklo_pd(y8, y9);
        _mm256_stream_pd((double *)(out + 4*COL_SIZE + 0x00), y12);
        y13 = _mm256_unpacklo_pd(y10, y11);
        _mm256_stream_pd((double *)(out + 4*COL_SIZE + 0x20), y13);
        y12 = _mm256_unpackhi_pd(y8, y9);
        _mm256_stream_pd((double *)(out + 5*COL_SIZE + 0x00), y12);
        y13 = _mm256_unpackhi_pd(y10, y11);
        _mm256_stream_pd((double *)(out + 5*COL_SIZE + 0x20), y13);

        // Transpose second two columns
        y8 = _mm256_permute2f128_pd(y0, y2, 0x31);
        y9 = _mm256_permute2f128_pd(y1, y3, 0x31);
        y10 = _mm256_permute2f128_pd(y4, y6, 0x31);
        y11 = _mm256_permute2f128_pd(y5, y7, 0x31);
        y12 = _mm256_unpacklo_pd(y8, y9);
        _mm256_stream_pd((double *)(out + 6*COL_SIZE + 0x00), y12);
        y13 = _mm256_unpacklo_pd(y10, y11);
        _mm256_stream_pd((double *)(out + 6*COL_SIZE + 0x20), y13);
        y12 = _mm256_unpackhi_pd(y8, y9);
        _mm256_stream_pd((double *)(out + 7*COL_SIZE + 0x00), y12);
        y13 = _mm256_unpackhi_pd(y10, y11);
        _mm256_stream_pd((double *)(out + 7*COL_SIZE + 0x20), y13);

        in  += 8*ROW_SIZE;
        out += 0x40;
        if (++counti == ROWS/8)
        {
            in -= IN_MATRIX_SIZE;
            in += 0x40;
            out += 7*COL_SIZE;
            counti = 0;
        }
    }
}

