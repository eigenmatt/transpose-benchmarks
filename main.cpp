#include <benchmark/benchmark.h>

#include <cstring>
#include <math.h>
#include <fcntl.h>
#include <unistd.h>
#include <iostream>

// Benchmarking 2^12 x 2^11th matrices (Hermez)

static const size_t matrix_columns = 672;
static const size_t matrix_rows = 2048 * 4096;
//static const size_t matrix_rows = 512 * 1024;

// region - memcpy

static void memcpy_bench(benchmark::State& state) {
  srand(time(NULL));
  uint64_t* input_matrix = (uint64_t *)valloc(sizeof(uint64_t) * matrix_columns * matrix_rows);
  uint64_t* output_matrix = (uint64_t *)valloc(sizeof(uint64_t) * matrix_rows * matrix_columns);

  int rand = open("/dev/urandom", O_RDONLY);
  ssize_t result = read(rand, input_matrix, sizeof(uint64_t) * matrix_columns * matrix_rows);
  assert(result != sizeof(uint64_t) * matrix_columns * matrix_rows);

  for (auto _: state) {
    memcpy(output_matrix, input_matrix, sizeof(uint64_t) * matrix_columns * matrix_rows);
    benchmark::ClobberMemory();
  }

  close(rand);
  free(output_matrix);
  free(input_matrix);
}
BENCHMARK(memcpy_bench)->Name("memcpy")->UseRealTime()->Unit(benchmark::kSecond);
//->ThreadRange(1, 16)

// endregion

// region - ulvt_transpose

#include "ulvt-transpose.h"

static void ulvt_transpose_v1_bench(benchmark::State& state) {
  srand(time(NULL));
  uint64_t* input_matrix = (uint64_t *)valloc(sizeof(uint64_t) * matrix_columns * matrix_rows);
  uint64_t* output_matrix = (uint64_t *)valloc(sizeof(uint64_t) * matrix_rows * matrix_columns);

  int rand = open("/dev/urandom", O_RDONLY);
  ssize_t result = read(rand, input_matrix, sizeof(uint64_t) * matrix_columns * matrix_rows);
  assert(result != sizeof(uint64_t) * matrix_columns * matrix_rows);

  for (auto _: state) {
    ulvt_transpose_v1(output_matrix, input_matrix);
    benchmark::ClobberMemory();
  }

  close(rand);
  free(output_matrix);
  free(input_matrix);
}
BENCHMARK(ulvt_transpose_v1_bench)->Name("ulvt_transpose_v1")->UseRealTime()->Unit(benchmark::kSecond);
//->ThreadRange(1, 16)

// endregion

//region - MKL

#include "mkl.h"

static void MKL(benchmark::State& state) {
  mkl_set_num_threads_local(state.range(0));
  uint64_t* input_matrix = (uint64_t *)valloc(sizeof(uint64_t) * matrix_columns * matrix_rows);
  uint64_t* output_matrix = (uint64_t *)valloc(sizeof(uint64_t) * matrix_rows * matrix_columns);

  int rand = open("/dev/urandom", O_RDONLY);
  ssize_t result = read(rand, input_matrix, sizeof(uint64_t) * matrix_columns * matrix_rows);
  assert(result != sizeof(uint64_t) * matrix_columns * matrix_rows);

  for (auto _: state) {
    mkl_domatcopy(
      'r', 
      't', 
      matrix_rows,
      matrix_columns,
      1.0, 
      (double *)input_matrix,
      matrix_columns,
      (double *)output_matrix,
      matrix_rows
      );
  }

#ifdef DEBUG
  for (size_t i = 0; i < matrix_columns; i++) {
    for (size_t j = 0; j < matrix_rows; j++) {
      assert(output_matrix[i * matrix_rows + j] == input_matrix[j * matrix_columns + i]);
    }
  }
#endif

  close(rand);
  free(output_matrix);
  free(input_matrix);
}
BENCHMARK(MKL)->RangeMultiplier(2)->Range(1, 16)->UseRealTime()->Unit(benchmark::kSecond);

//endregion

// region - eigen

#include <Eigen/Dense>

typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> HermezMatrix;

static void EigenTranspose(benchmark::State& state) {
  Eigen::setNbThreads(state.range(0));
  HermezMatrix input_matrix = HermezMatrix::Random(matrix_rows, matrix_columns);
  HermezMatrix output_matrix = HermezMatrix::Zero(matrix_columns, matrix_rows);
  for (auto _: state) {
    output_matrix = input_matrix.transpose();
    benchmark::ClobberMemory();
  }

#ifdef DEBUG
  for (size_t i = 0; i < matrix_columns; i++) {
    for (size_t j = 0; j < matrix_rows; j++) {
      assert(output_matrix(i, j) == input_matrix(j, i));
    }
  }
#endif
}
BENCHMARK(EigenTranspose)->Name("Eigen")->RangeMultiplier(2)->Range(1, 16)->UseRealTime()->Unit(benchmark::kSecond);

//endregion

// region - fftw

#include "fftw-shim.h"

shim_fftw_plan plan_transpose(char storage_type,
                         int rows,
                         int cols,
                         double *in,
                         double *out)
{
    const unsigned flags = FFTW_MEASURE | FFTW_DESTROY_INPUT;

    shim_fftw_iodim howmany_dims[2];
    switch (toupper(storage_type)) {
        case 'R':
            howmany_dims[0].n  = rows;
            howmany_dims[0].is = cols;
            howmany_dims[0].os = 1;
            howmany_dims[1].n  = cols;
            howmany_dims[1].is = 1;
            howmany_dims[1].os = rows;
            break;
        case 'C':
            howmany_dims[0].n  = rows;
            howmany_dims[0].is = 1;
            howmany_dims[0].os = cols;
            howmany_dims[1].n  = cols;
            howmany_dims[1].is = rows;
            howmany_dims[1].os = 1;
            break;
        default:
            return NULL;
    }
    const int howmany_rank = sizeof(howmany_dims)/sizeof(howmany_dims[0]);

    return shim_fftw_plan_guru_r2r(/*rank*/0, /*dims*/NULL,
                              howmany_rank, howmany_dims,
                              in, out, /*kind*/NULL, flags);
}

static void fftw(benchmark::State& state) {
  shim_fftw_init_threads();
  shim_fftw_plan_with_nthreads(state.range(0));
  uint64_t* plan_input_matrix = (uint64_t *)shim_fftw_malloc(sizeof(uint64_t) * matrix_columns * matrix_rows);
  uint64_t* plan_output_matrix = (uint64_t *)shim_fftw_malloc(sizeof(uint64_t) * matrix_rows * matrix_columns);
  uint64_t* input_matrix = (uint64_t *)shim_fftw_malloc(sizeof(uint64_t) * matrix_columns * matrix_rows);
  uint64_t* output_matrix = (uint64_t *)shim_fftw_malloc(sizeof(uint64_t) * matrix_rows * matrix_columns);

  char filename[256];
  sprintf(filename, "bench_%ld_%s.wis", state.range(0), __TIME__);

  int wisdom_imported = shim_fftw_import_wisdom_from_filename(filename);

  fftw_plan plan = plan_transpose('R', matrix_rows, matrix_columns, (double*)plan_input_matrix, (double*)plan_output_matrix);

  assert(plan != NULL);

  if (!wisdom_imported) {
    shim_fftw_export_wisdom_to_filename(filename);
  }

  int rand = open("/dev/urandom", O_RDONLY);
  ssize_t result = read(rand, input_matrix, sizeof(uint64_t) * matrix_columns * matrix_rows);
  assert(result != sizeof(uint64_t) * matrix_columns * matrix_rows);

  for (auto _: state) {
    shim_fftw_execute_r2r(plan, (double*)input_matrix, (double*)output_matrix);
    benchmark::ClobberMemory();
    //fftw_execute(plan);
  }

#ifdef DEBUG
  for (size_t i = 0; i < matrix_columns; i++) {
    for (size_t j = 0; j < matrix_rows; j++) {
      //assert(plan_output_matrix[i * matrix_rows + j] == plan_input_matrix[j * matrix_columns + i]);
      assert(output_matrix[i * matrix_rows + j] == input_matrix[j * matrix_columns + i]);
    }
  }
#endif

  close(rand);
  shim_fftw_free(output_matrix);
  shim_fftw_free(input_matrix);
  shim_fftw_free(plan_output_matrix);
  shim_fftw_free(plan_input_matrix);
}
BENCHMARK(fftw)->RangeMultiplier(2)->Range(1, 16)->UseRealTime()->Unit(benchmark::kSecond);

// endregion

// region - hptt

#include <iostream>
#include <hptt.h>

static void hptt_bench(benchmark::State& state) {
  srand(time(NULL));
  int64_t* input_matrix = (int64_t *)valloc(sizeof(int64_t) * matrix_columns * matrix_rows);
  int64_t* output_matrix = (int64_t *)valloc(sizeof(int64_t) * matrix_rows * matrix_columns);

  int dim = 2;
  int perm[dim] = {1, 0};
  int size[dim] = {matrix_rows, matrix_columns};
  auto plan = hptt::create_plan(perm, dim, 1.0, (int64_t*)input_matrix, size, NULL, 0.0, (int64_t*)output_matrix, NULL, hptt::ESTIMATE, state.range(0), nullptr, true);

  int rand = open("/dev/urandom", O_RDONLY);
  ssize_t result = read(rand, input_matrix, sizeof(uint64_t) * matrix_columns * matrix_rows);
  assert(result != sizeof(uint64_t) * matrix_columns * matrix_rows);

  for (auto _: state) {
    plan->execute();
    benchmark::ClobberMemory();
  }

#ifdef DEBUG
  for (size_t i = 0; i < matrix_columns; i++) {
    for (size_t j = 0; j < matrix_rows; j++) {
      //assert(plan_output_matrix[i * matrix_rows + j] == plan_input_matrix[j * matrix_columns + i]);
      if (output_matrix[i * matrix_rows + j] != input_matrix[j * matrix_columns + i]) {
        std::cout << "index " << i << ',' << j << " incorrect" << std::endl;
        std::cout << "input is " << input_matrix[j*matrix_columns+i] << " (" << ((double*)(input_matrix))[j*matrix_columns+i] << ")" << std::endl;
        std::cout << "output is " << output_matrix[i * matrix_rows + j] << " (" << ((double*)(output_matrix))[i * matrix_rows + j] << ")" << std::endl;
      }
      assert(output_matrix[i * matrix_rows + j] == input_matrix[j * matrix_columns + i]);
    }
  }
#endif

  close(rand);
  free(output_matrix);
  free(input_matrix);
}
BENCHMARK(hptt_bench)->Name("hptt")->RangeMultiplier(2)->Range(1, 16)->UseRealTime()->Unit(benchmark::kSecond);

// endregion

BENCHMARK_MAIN();
